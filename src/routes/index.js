import express from "express";

const router = express.Router();

router.get("/me", (req, res) => {
  res.status(200).json({
    status: 200,
    msg: "API Test routes",
    result: {}
  });
});

export default router;

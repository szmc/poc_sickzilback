import express from "express";
import { uploadImage } from "../controller/storage";

const router = express.Router();

router.route("/images").post(uploadImage);

export default router;

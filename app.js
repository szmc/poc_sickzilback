import { config } from "dotenv";
import express from "express";
import indxRouter from "./src/routes";
import bodyParser from "body-parser";

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser);
const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Express server has opened on port ${port}!`);
});

app.use("/api", indxRouter);
